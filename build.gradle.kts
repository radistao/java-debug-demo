plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.60"
    application
}

repositories {
    jcenter()
    mavenCentral()
}

application {
    mainClassName = "org.radistao.DebugAppKt"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    jar {
        manifest {
            attributes(("Main-Class" to application.mainClassName))
        }

        doFirst { println("Assemble Fat jar") }

        from(sourceSets.main.get().output)

        dependsOn(configurations.runtimeClasspath)
        from({
            configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
        })
    }
}


dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    implementation("com.sparkjava:spark-core:2.9.1")
    runtimeOnly("org.slf4j:slf4j-simple:1.7.29")
}
