# docker build -t java-debug-demo .
# docker run --name java-debug-demo --rm -it -p8080:8080 -p5005:5005 --env JAVA_TOOL_OPTIONS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005" java-debug-demo
#   curl localhost:8080
#   curl 192.168.99.100:8080
# docker tag java-debug-demo radistao/java-debug-demo
# docker push radistao/java-debug-demo

FROM adoptopenjdk/openjdk11:alpine
COPY build/libs/java-debug-demo.jar /app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]