package org.radistao

import java.net.InetAddress

import java.lang.String.format
import spark.Spark.get
import spark.Spark.port

fun main(args: Array<String>) {
    port(8080)
    get("/") { _, _ ->
        val localHost = InetAddress.getLocalHost()

        val canonicalHostName = localHost.canonicalHostName
        val hostAddress = localHost.hostAddress
        val hostName = localHost.hostName

        format("Hello from %s (%s), %s", hostName, canonicalHostName, hostAddress)
    }
}
